module.exports = {
    url: 'https://www.flipkart.com',
    elements: {
        closeModal: { 
            selector: "//*[contains(@class,'_2KpZ6l _2doB4z')]", 
            locateStrategy: 'xpath' 
        },
        searchBar: {
            selector: "//input[@name='q']",
            locateStrategy: 'xpath'
        },
        searchButton: {
            selector: "//*[contains(@class, 'L0Z3Pu')]",
            locateStrategy: 'xpath'
        },
        sortLowToHighButton: {
            selector: "//div[contains(@class, '_10UF8M') and text() = 'Price -- Low to High']",
            locateStrategy: 'xpath'
        },
        productName: {
            selector: "//*[contains(@class, 's1Q9rs')]",
            locateStrategy: 'xpath'
        },
        pincodeInput: {
            selector: "//input[@id='pincodeInputId']",
            locateStrategy: 'xpath'
        },
        checkPincodeButton: {
            selector: "//*[contains(@class, '_2P_LDn')]",
            locateStrategy: 'xpath'
        },
        addToCart: {
            selector: "//*[contains(@class, '_2KpZ6l _2U9uOA _3v1-ww')]",
            locateStrategy: 'xpath'
        }
    },
    commands: [{
        
    }]
};
