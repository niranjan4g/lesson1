module.exports = {
  
    'step one: navigate to flipkart' : function (browser) { 
      const page = browser.page.xpaths();
      const mainQuery = 'Play Station';
      const pincode = '751024';
      var prices = new Array();
     
      page
        .navigate()
        .waitForElementVisible('body', 2000)
        .useXpath().click("@closeModal")
        .pause(1000)
        .assert.visible("@searchBar")
        .setValue("@searchBar", mainQuery)
        .pause(1000)
        .useXpath().click("@searchButton")
        .pause(1000)
        .assert.containsText("@sortLowToHighButton", "Price -- Low to High")
        .useXpath().click("@sortLowToHighButton")
        .pause(10000);
      
      browser
          .elements('css selector', '._30jeq3', function (result) {
           
            for (var i = 0; i < result.value.length; i++) {
              var element = result.value[i];
              browser
              .elementIdText(element.ELEMENT, function(anchor){
                var res = anchor.value.slice(1);
                
                prices.push(res);
               
                var lengthOfArray = prices.length;
                var currentPrice = prices[0];
                var nextPrice = prices[lengthOfArray-1];
                //code to check first price with the next price and continued
                if(currentPrice <= nextPrice){
                 console.log("Sorting is Proper: Low to High");
                }else{
                  console.log("Sorting is not Proper: Low to High")
                }
              })
            }
          
          });

      page
        .pause(1000)
        .useXpath().click("@productName")
        .pause(1000);

      browser
        .windowHandles(function (result) {
          var handle = result.value[1]
          browser.switchWindow(handle)
          browser.pause(1000)
        });

        page
         .assert.visible("@pincodeInput")
         .setValue("@pincodeInput", pincode)
         .pause(1000)
         .useXpath().click("@checkPincodeButton")
         .pause(1000)
         .useXpath().click("@addToCart")
         .pause(1000)

        browser
         .pause(1000)
         .end(); 
    }
};